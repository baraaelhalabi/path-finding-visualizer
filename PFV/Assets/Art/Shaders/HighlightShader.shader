Shader "Unlit/Highlight" {
    Properties {
        _StartColor("Start Color", Color) = (1, 1, 1, 1)
        _EndColor("End Color", Color) = (1, 1, 1, 1)
        _FadeDuration("Fade Duration", Float) = 1
    }
    SubShader {
        Tags { "RenderType"="Opaque" }

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            float4 _StartColor;
            float4 _EndColor;
            float _FadeDuration;

            float clamp(float val, float max) {
                if(val > max) return max;
                return val;
            }
            
            struct MeshData {
                float4 vertex : POSITION;
                float2 uv1 : TEXCOORD1;
            };

            struct Interpolators {
                float4 vertex : SV_POSITION;
                float2 time : TEXCOORD1;
            };

            Interpolators vert (MeshData v) {
                Interpolators o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.time = v.uv1;
                return o;
            }

            float4 frag (Interpolators i) : SV_Target {
                return lerp(_StartColor, _EndColor, clamp(_Time.y - i.time.x, 1));
            }
            ENDCG
        }
    }
}
