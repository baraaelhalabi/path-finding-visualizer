using System.Collections.Generic;
using OSM.DataStructures;
using Singleton;
using UnityEngine;

namespace OSM.Renderer {
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class OsmRenderer : MonoBehaviourSingleton<OsmRenderer> {
        [Header("Renderer Settings")]
        public float thickness;
        public Material material;

        private MeshFilter _meshFilter;
        private MeshRenderer _meshRenderer;

        protected override void SingletonAwake() {
            _meshFilter = GetComponent<MeshFilter>();
            _meshRenderer = GetComponent<MeshRenderer>();
            
            //TODO refactor this later
            SetMaterial(material);
        }

        public void Render(OpenStreetMap map) {
            var mesh = GenerateMesh(map);
            _meshFilter.mesh = mesh;
        }

        private Mesh GenerateMesh(OpenStreetMap map) {
            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var mesh = new Mesh();

            var segments = 0;
        
            foreach (var way in map.Ways) {
                for (var i = 0; i < way.Ids.Count - 1; i++) {
                    var p1 = map.WorldPosition(map.Nodes[way.Ids[i]]);
                    var p2 = map.WorldPosition(map.Nodes[way.Ids[i + 1]]);
                    var rotation = Quaternion.LookRotation(p2 - p1).normalized;
                
                    //add vertices
                    vertices.Add(p1 + rotation * Vector3.left * thickness / 2f);
                    vertices.Add(p1 + rotation * Vector3.right * thickness / 2f);
                    vertices.Add(p2 + rotation * Vector3.left * thickness / 2f);
                    vertices.Add(p2 + rotation * Vector3.right * thickness / 2f);
                
                    //add triangles
                    triangles.Add(0 + 4 * segments);
                    triangles.Add(2 + 4 * segments);
                    triangles.Add(1 + 4 * segments);
                    triangles.Add(1 + 4 * segments);
                    triangles.Add(2 + 4 * segments);
                    triangles.Add(3 + 4 * segments);
                
                    segments++;
                }
            }
            
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            return mesh;
        }

        private void SetMaterial(Material mat) {
            material = mat;
            _meshRenderer.material = material;
        }
    }
}
