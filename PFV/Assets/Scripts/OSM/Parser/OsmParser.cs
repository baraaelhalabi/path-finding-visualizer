using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using OSM.DataStructures;

namespace OSM.Parser {
    public abstract class OsmParser {
        public static OpenStreetMap Parse(XmlDocument doc) {
            return new OpenStreetMap(
                LoadNodes(doc.SelectNodes("/osm/node")),
                LoadWays(doc.SelectNodes("/osm/way")),
                LoadBounds(doc.SelectSingleNode("/osm/bounds"))
            );
        }

        private static OsmBounds LoadBounds(XmlNode node) {
            return new OsmBounds(node);
        }

        private static Dictionary<ulong, OsmNode> LoadNodes(XmlNodeList xmlNodes) {
            var nodes = new Dictionary<ulong, OsmNode>();
            foreach (XmlNode node in xmlNodes) {
                var n = new OsmNode(node);
                nodes[n.Id] = n;
            }
            return nodes;
        }

        private static List<OsmWay> LoadWays(IEnumerable nodes) {
            return (from XmlNode node in nodes select new OsmWay(node)).ToList();
        }
    }
}
