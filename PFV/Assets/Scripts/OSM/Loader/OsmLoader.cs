using System.Xml;
using OSM.DataStructures;
using OSM.Parser;
using UnityEngine;

namespace OSM.Loader {
    public abstract class OsmLoader {
        public static OpenStreetMap LoadMap(TextAsset file) {
            var document = new XmlDocument();
            document.LoadXml(file.text);
            return OsmParser.Parse(document);
        }
    }
}
