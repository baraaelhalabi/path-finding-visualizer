using System.Xml;
using UnityEngine;
using Utility;
using static Utility.MercatorProjection;

namespace OSM.DataStructures {
    public class OsmNode {
        public ulong Id { get; private set; }
        public float Latitude { get; private set; }
        public float Longitude { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        
        public static implicit operator Vector3 (OsmNode node) {
            return new Vector3(node.X, 0, node.Y);
        }

        public OsmNode(XmlNode node) {
            Id = node.Attributes.GetAttribute<ulong>("id");
            Latitude = node.Attributes.GetAttribute<float>("lat");
            Longitude = node.Attributes.GetAttribute<float>("lon");
            X = (float)LonToX(Longitude);
            Y = (float)LatToY(Latitude);
        }
    }
}