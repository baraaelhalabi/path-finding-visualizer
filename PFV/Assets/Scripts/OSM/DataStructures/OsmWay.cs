using System.Collections.Generic;
using System.Xml;
using Utility;

namespace OSM.DataStructures {
    public class OsmWay {
        public ulong Id { get; private set; }
        public bool Visible { get; private set; }
        public List<ulong> Ids { get; private set; }
        public bool IsBoundary { get; private set; }
        
        public OsmWay(XmlNode node) {
            var isSquare = false;
            
            Ids = new List<ulong>();
            
            Id = node.Attributes.GetAttribute<ulong>("id");
            Visible = node.Attributes.GetAttribute<bool>("visible");

            var nodes = node.SelectNodes("nd");
            if (nodes == null) return;
            foreach (XmlNode n in nodes) {
                var reference = n.Attributes.GetAttribute<ulong>("ref");
                Ids.Add(reference);
            }

            var tags = node.SelectNodes("tag");
            if (tags != null) {
                foreach (XmlNode tag in tags) {
                    var type = tag.Attributes.GetAttribute<string>("v");
                    if (type is "square" or "roundabout")
                        isSquare = true;
                }
            }

            if (Ids.Count > 1) IsBoundary = Ids[0] == Ids[^1] && !isSquare;
            
            //this is added to currently pass empty data in the case of a closed path e.g. buildings
            if (IsBoundary) {
                Ids.Clear();
            }
        }
    }
}
