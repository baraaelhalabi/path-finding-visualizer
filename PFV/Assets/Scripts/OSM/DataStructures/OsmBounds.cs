using UnityEngine;
using System.Xml;
using Utility;
using static Utility.MercatorProjection;

namespace OSM.DataStructures {
    public class OsmBounds {
        public float MinLat { get; private set; }
        public float MaxLat { get; private set; }
        public float MinLon { get; private set; }
        public float MaxLon { get; private set; }
        public float LeftEdge { get; private set; }
        public float RightEdge { get; private set; }
        public float UpperEdge { get; private set; }
        public float LowerEdge { get; private set; }
        public Vector3 Center { get; private set; }

        public OsmBounds(XmlNode node) {
            MinLat = node.Attributes.GetAttribute<float>("minlat");
            MaxLat = node.Attributes.GetAttribute<float>("maxlat");
            MinLon = node.Attributes.GetAttribute<float>("minlon");
            MaxLon = node.Attributes.GetAttribute<float>("maxlon");

            LeftEdge = (float)LonToX(MinLon);
            RightEdge = (float)LonToX(MaxLon);
            LowerEdge = (float)LonToX(MinLat);
            UpperEdge = (float)LonToX(MaxLat);

            var x = (float)((LonToX(MaxLon) + LonToX(MinLon)) / 2);
            var y = (float)((LatToY(MaxLat) + LatToY(MinLat)) / 2);

            Center = new Vector3(x, 0, y);
        }
    }
}
