using System.Collections.Generic;
using UnityEngine;

namespace OSM.DataStructures {
    public class OpenStreetMap {
        public Dictionary<ulong, OsmNode> Nodes { get; private set; }
        public List<OsmWay> Ways { get; private set; }
        public OsmBounds Bounds { get; private set; }

        public Vector3 WorldPosition(OsmNode node) {
            return node - Bounds.Center;
        }

        public OpenStreetMap(Dictionary<ulong, OsmNode> nodes, List<OsmWay> ways, OsmBounds bounds) {
            Nodes = nodes;
            Ways = ways;
            Bounds = bounds;
        }
    }
}
