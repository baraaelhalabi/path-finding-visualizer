using UnityEngine;

namespace Singleton {
    public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour {
        [Header("Singleton Settings")]
        public bool dontDestroyOnLoad = true;

        public static T Instance { get; private set; }

        private void Awake() {
            if (Instance is not null && Instance != this as T) {
                Destroy(gameObject);
                return;
            }

            Instance = this as T;

            if (dontDestroyOnLoad)
                DontDestroyOnLoad(gameObject);
            
            SingletonAwake();
        }

        protected virtual void SingletonAwake() { }
    }
}
