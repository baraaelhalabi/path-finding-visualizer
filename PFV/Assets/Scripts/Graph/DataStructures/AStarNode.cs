using System.Collections.Generic;
using UnityEngine;

namespace Graph.DataStructures {
    public class AStarNode : Node {
        public float GCost { get; set; }
        public float HCost { get; set; }
        public float FCost => GCost + HCost;
        
        public AStarNode(string id, Vector3 position, List<Node> children = null) : base(id, position, children) {
        }
    }
}
