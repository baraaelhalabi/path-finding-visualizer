using System.Collections.Generic;
using System.Linq;

namespace Graph.DataStructures {
    public class Graph {
        public List<Node> Nodes { get; private set; }
        public int Edges { get; private set; }

        public Graph(List<Node> nodes, int edges) {
            Nodes = nodes;
            Edges = edges;
        }

        public Node GetById(string id) {
            return Nodes.FirstOrDefault(node => node.Id.Trim() == id.Trim());
        }

        public void Reset() {
            foreach (var node in Nodes) {
                node.Visited = false;
                node.Weight = -1;
                node.Source = null;
            }
        }
    }
}
