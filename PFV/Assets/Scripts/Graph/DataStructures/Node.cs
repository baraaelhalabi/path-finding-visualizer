using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Graph.DataStructures {
    public class Node {
        public string Id { get; protected set; }
        public Vector3 Position { get; protected set; }
        public List<Node> Children { get; protected set; }
        public Node Source { get; set; } = null;
        public bool Visited { get; set; }
        public float Weight { get; set; } = -1;
        public object Data { get; set; }

        public Vector3 WorldPosition { get; set; }

        public override string ToString() {
            return $"{Id} Position: [{Position.x}, {Position.z}] | Weight: {Weight}\nChildren:\n {string.Join(", ", Children.Select(child => child.Id))}";
        }

        public Node(string id, Vector3 position, List<Node> children = null) {
            Id = id;
            Position = position;
            Children = children ?? new List<Node>();
        }

        public void AddChildren(params Node[] children) {
            foreach (var node in children) {
                Children.Add(node);
            }
        }

        public int CompareTo(Node other) {
            return Weight.CompareTo(other.Weight);
        }
    }
}
