using System.Collections.Generic;
using System.Linq;
using Graph.DataStructures;
using OSM.DataStructures;

namespace Graph.Builder {
    public abstract class GraphBuilder {
        public static DataStructures.Graph ToGraph(OpenStreetMap map) {
            var nodeDic = new Dictionary<ulong, Node>();
            var edges = 0;
            foreach (var edge in map.Ways) {
                for (var i = 0; i < edge.Ids.Count - 1; i++) {
                    if (edge.IsBoundary) continue;
                    var currentNodeId = edge.Ids[i];
                    var nextNodeId = edge.Ids[i + 1];
                    //checks if the node exists in the dictionary
                    if (!nodeDic.ContainsKey(currentNodeId)) {
                        nodeDic[currentNodeId] =
                            new Node($"{currentNodeId}", map.WorldPosition(map.Nodes[currentNodeId])) {
                                WorldPosition = map.WorldPosition(map.Nodes[currentNodeId])
                            };
                    }
                    if (!nodeDic.ContainsKey(nextNodeId)) {
                        nodeDic[nextNodeId] =
                            new Node($"{nextNodeId}", map.WorldPosition(map.Nodes[nextNodeId])) {
                                WorldPosition = map.WorldPosition(map.Nodes[nextNodeId])
                            };
                    }
                    
                    nodeDic[currentNodeId].Children.Add(nodeDic[nextNodeId]);
                    nodeDic[nextNodeId].Children.Add(nodeDic[currentNodeId]);
                    edges++;
                }
            }

            var nodes = nodeDic.Select(node => node.Value).ToList();
            
            return new DataStructures.Graph(nodes, edges);
        }
    }
}