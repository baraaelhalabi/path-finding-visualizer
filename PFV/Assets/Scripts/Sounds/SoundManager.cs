using Singleton;
using UnityEngine;

namespace Sounds {
    public class SoundManager : MonoBehaviourSingleton<SoundManager> {
        [Header("Audio Sources")]
        public AudioSource exploreAudioSource;
        public AudioSource mapLoadAudioSource;
        public AudioSource solutionAudioSource;

        private float _pitch = 2f;
        public void PlayExploreSound() {
            exploreAudioSource.pitch = _pitch;
            _pitch += 0.0005f;
            if (exploreAudioSource.isPlaying) return;
            exploreAudioSource.Play();
        }

        public void PlaySolutionSoundEffect() {
            exploreAudioSource.Stop();
            solutionAudioSource.Play();
        }

        public void PlayMapLoadSoundEffect() {
            mapLoadAudioSource.Play();
        }

        public void Reset() {
            _pitch = 2f;
        }
    }
}
