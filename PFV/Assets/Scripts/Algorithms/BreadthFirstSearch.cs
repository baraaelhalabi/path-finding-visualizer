using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Graph.DataStructures;
using Visual;

namespace Algorithms {
    public class BreadthFirstSearch : IPathFinder {
        public Task Solve(Node source, Node target) {
            var nodes = new Queue<Node>();
            nodes.Enqueue(source);
            source.Visited = true;
            while (nodes.Count > 0) {
                var currentNode = nodes.Dequeue();
                if(currentNode.Source != null)
                    Visualizer.Instance.AddStep(currentNode.Source, currentNode);
                //solution is found
                if (currentNode == target) {
                    var path = new List<Node> { currentNode };
                    var parent = currentNode.Source;
                    while (parent != null) {
                        path.Add(parent);
                        parent = parent.Source;
                    }
                    path.Reverse();
                    for (var i = 0; i < path.Count - 1; i ++)
                        Visualizer.Instance.AddSolutionStep(path[i], path[i + 1]);
                    return Task.CompletedTask;
                }
                foreach (var node in currentNode.Children.Where(node => !node.Visited)) {
                    //set the source of visiting the child node
                    node.Source = currentNode;
                    node.Visited = true;
                    nodes.Enqueue(node);
                }
            }
            return Task.CompletedTask;
        }
    }
}
