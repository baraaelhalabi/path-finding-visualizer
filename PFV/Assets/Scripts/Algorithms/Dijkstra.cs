using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Graph.DataStructures;
using UnityEngine;
using Visual;

namespace Algorithms {
    public class Dijkstra : IPathFinder {
        public Task Solve(Node source, Node target) {
            source.Weight = 0;
            var currentNode = source;
            var nodes = new List<Node> {currentNode};
            while (nodes.Count > 0) {
                //visualization
                if (currentNode.Source != null)
                     Visualizer.Instance.AddStep(currentNode.Source, currentNode);
                //check if solution is found
                if (currentNode == target) {
                    Debug.Log("Found Solution");
                    var parent = currentNode.Source;
                    var path = new List<Node> { currentNode }; 
                    while (parent != null) {
                        path.Add(parent); 
                        parent = parent.Source;
                    }
                    path.Reverse();
                    for (var i = 0; i < path.Count - 1; i ++) {
                        Visualizer.Instance.AddSolutionStep(path[i], path[i + 1]);
                    }
                    Debug.Log($"<color=green>{string.Join(" | ", path.Select(node => node.Id))}</color>");
                    return Task.CompletedTask;
                }
                var toExplore = EvaluateNeighbours(currentNode).ToList();
                var children = toExplore.Aggregate("", (current, n) => current + $"{n}\n");
                Debug.Log($"Children: \n{children}");
                nodes.AddRange(toExplore);
                nodes.Remove(currentNode);
                //check if solution is impossible
                if (nodes.Count == 0) {
                    Debug.Log("No solution");
                    return Task.FromResult(Enumerable.Empty<Node>().ToList());
                }
                currentNode.Visited = true;
                currentNode = GetMinCostNeighbour(nodes);
            }
    
            Debug.Log("finished");
            return Task.CompletedTask;
        }
    
        private IEnumerable<Node> EvaluateNeighbours(Node node) {
            var exploredNodes = new List<Node>();
            foreach (var child in node.Children) {
                if (child.Visited) continue;
                var weight = node.Weight + Vector2.Distance(node.Position, child.Position);
                if(child.Weight < 0) exploredNodes.Add(child);
                if (child.Weight <= weight && child.Weight >= 0) continue;
                
                child.Weight = weight;
                child.Source = node;
            }
            return exploredNodes;
        }
    
        private Node GetMinCostNeighbour(IReadOnlyList<Node> nodes) {
            var node = nodes[0];
            return nodes.Aggregate(node, (current, n) => current.Weight < n.Weight ? current : n);
        }
    }
}
