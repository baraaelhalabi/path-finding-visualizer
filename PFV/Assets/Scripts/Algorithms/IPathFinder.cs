using System.Threading.Tasks;
using Graph.DataStructures;

namespace Algorithms {
    public interface IPathFinder {
        public Task Solve(Node source, Node target);
    }
}
