using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Graph.DataStructures;
using UnityEngine;
using Visual;

namespace Algorithms {
    [Serializable]
    public struct AStarData {
        public float GCost { get; set; }
        public float HCost { get; set; }
        public float FCost => GCost + HCost;
    }
    
    public class AStar : IPathFinder {
        public Task Solve(Node source, Node target) {
            Debug.Log("Solving with A*");
            var currentNode = source;
            var nodes = new List<Node> { source };
            while (nodes.Count > 0) {
                if (currentNode.Source != null)
                    Visualizer.Instance.AddStep(currentNode.Source, currentNode);
                
                Debug.Log($"Checking Node: {currentNode.Id}");
                if (currentNode == target) {
                    Debug.Log("<color=green>Solution found!</color>");
                    
                    var parent = currentNode.Source;
                    var path = new List<Node> { currentNode }; 
                    while (parent != null) {
                        path.Add(parent); 
                        parent = parent.Source;
                    }
                    path.Reverse();
                    for (var i = 0; i < path.Count - 1; i ++) {
                        Visualizer.Instance.AddSolutionStep(path[i], path[i + 1]);
                    }
                    return Task.CompletedTask;
                }
                nodes.AddRange(EvaluateNeighbours(currentNode, target));
                currentNode.Visited = true;
                nodes.Remove(currentNode);
                currentNode = GetMinCostNeighbour(nodes);
                // SetSources(currentNode);
            }
            
            return Task.CompletedTask;
        }

        private IEnumerable<Node> EvaluateNeighbours(Node node, Node target) {
            var exploredNodes = new List<Node>();
            foreach (var child in node.Children.Where(child => !child.Visited)) {
                var data = new AStarData {
                    GCost = Vector3.Distance(node.WorldPosition, child.WorldPosition),
                    HCost = Vector3.Distance(child.WorldPosition, target.WorldPosition)
                };
                child.Source = node;
                child.Data = data;
                exploredNodes.Add(child);
            }
            return exploredNodes;
        }

        private Node GetMinCostNeighbour(List<Node> nodes) {
            var minFCost = Data(nodes[0]).FCost;
            minFCost = nodes.Select(node => Data(node).FCost).Prepend(minFCost).Min();
            var minFCostNodes = nodes.FindAll(node => Math.Abs(Data(node).FCost - minFCost) < double.Epsilon);
            return minFCostNodes.OrderBy(node => Data(node).HCost).FirstOrDefault();
        }

        private static AStarData Data(Node node) {
            return (AStarData)node.Data;
        }
    }
}
