using System;
using Algorithms;
using Core;
using Sounds;
using TMPro;
using UI.ViewManagement;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using Visual;

namespace UI.Views {
    public class SimulatorView : View {
        [Header("Navigation Buttons")]
        public Button backButton;
        
        [Header("Simulation Settings")]
        public TMP_Dropdown algorithmDropdown;
        public Slider simulationSpeedSlider;
        public Button solveButton;

        [Header("Sound Settings")]
        public Slider backgroundMusicSlider;
        public Slider soundEffectsSlider;

        [Header("Simulation Stats")]
        public TMP_Text exploredArea;
        public TMP_Text time;
        public TMP_Text distance;

        [Header("SideBar Properties")]
        public Button toggleButton;
        public Animator sideBarAnimator;

        [Header("Audio Mixer Groups")]
        public AudioMixerGroup backgroundMusicMixerGroup;
        public AudioMixerGroup soundEffectMixerGroup;

        [Header("Text Elements")]
        public TMP_Text fps;
        
        private static readonly int Open = Animator.StringToHash("Open");
        private static readonly int Close = Animator.StringToHash("Close");
        private bool _sidebarIsOpen;

        public override void Initialize() {
            //load saved values
            backgroundMusicSlider.value = PlayerPrefs.GetFloat("BackgroundMusicVolume", 1); 
            HandleBackgroundMusicChange(backgroundMusicSlider.value);
            soundEffectsSlider.value = PlayerPrefs.GetFloat("SoundEffectsVolume", 1);
            HandleSoundEffectsChange(soundEffectsSlider.value);
            HandleAlgorithmChange(0);

            StatsManager.Instance.OnEdgeExploredChanged += HandleExploredAreaChange;
            StatsManager.Instance.OnTimeChanged += HandleTimeChange;
            StatsManager.Instance.OnDistanceChange += HandleDistanceChange;
            
            backButton.onClick.AddListener(ViewManager.ShowLast);
            solveButton.onClick.AddListener(HandleSolve);
            simulationSpeedSlider.onValueChanged.AddListener(HandleSimulationSpeedChange);
            backgroundMusicSlider.onValueChanged.AddListener(HandleBackgroundMusicChange);
            soundEffectsSlider.onValueChanged.AddListener(HandleSoundEffectsChange);
            
            toggleButton.onClick.AddListener(ToggleSideBar);
            
            algorithmDropdown.onValueChanged.AddListener(HandleAlgorithmChange);
        }
        
        protected override void OnShow() {
            OpenSideBar();
            SoundManager.Instance.PlayMapLoadSoundEffect();
            StatsManager.Instance.Reset();
        }

        private void Update() {
            fps.text = $"FPS: {Time.frameCount / Time.time}";
        }

        private void HandleSolve() {
            CloseSideBar();
            AppManager.Instance.Solve();
        }

        private void HandleAlgorithmChange(int value) {
            AppManager.Instance.Algorithm = value switch {
                0 => new Dijkstra(),
                1 => new AStar(),
                2 => new BreadthFirstSearch(),
                3 => new DepthFirstSearch(),
                _ => throw new ArgumentException("Invalid Algorithm Index")
            };
        }

        private void HandleBackgroundMusicChange(float val) {
            var volume = Remap(val, 0, 1, -44, 0);
            backgroundMusicMixerGroup.audioMixer.SetFloat("BackgroundMusicVolume", volume);
            PlayerPrefs.SetFloat("BackgroundMusicVolume", val);
        }

        private void HandleSoundEffectsChange(float val) {
            var volume = Remap(val, 0, 1, -44, 0);
            soundEffectMixerGroup.audioMixer.SetFloat("SoundEffectsVolume", volume);
            PlayerPrefs.SetFloat("SoundEffectsVolume", val);
        }

        private void HandleSimulationSpeedChange(float val) {
            var mappedValue = Remap(val, 0, 1, 0.01f, 0.0001f);
            Visualizer.Instance.stepDuration = mappedValue;
        }
        
        private static float Remap(float value, float oldMin, float oldMax, float newMin, float newMax) {
            if (Math.Abs(oldMin - oldMax) < double.Epsilon || Math.Abs(newMin - newMax) < double.Epsilon)
                throw new ArgumentException("Old and new ranges must be non-zero.");
            
            var ratio = (value - oldMin) / (oldMax - oldMin);
            var newValue = newMin + ratio * (newMax - newMin);

            return newValue;
        }

        private void HandleExploredAreaChange(int exploredEdges) {
            var value = Remap(exploredEdges, 0, AppManager.Instance.Graph.Edges, 0, 100);
            exploredArea.text = $"Explored Area: {value:F1}%";
        }

        private void HandleTimeChange(float timeValue) {
            time.text = $"Simulation Time: {timeValue:F2}ms";
        }

        private void HandleDistanceChange(float distanceValue) {
            distance.text = $"Solution Distance: {(distanceValue/1000):F}km";
        }

        private void ToggleSideBar() {
            sideBarAnimator.SetTrigger(_sidebarIsOpen ? Close : Open);
            _sidebarIsOpen = !_sidebarIsOpen;
        }

        private void CloseSideBar() {
            sideBarAnimator.SetTrigger(Close);
            _sidebarIsOpen = false;
        }
        
        private void OpenSideBar() {
            sideBarAnimator.SetTrigger(Open);
            _sidebarIsOpen = true;
        }
    }
}
