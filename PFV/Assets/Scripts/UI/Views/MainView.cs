using System.Collections;
using System.Collections.Generic;
using Core;
using TMPro;
using UI.ViewManagement;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views {
    public class MainView : View {
        [Header("Buttons")]
        public Button startButton;

        [Header("Dropdown Menus")]
        public TMP_Dropdown mapDropdown;

        [Header("Blobs")]
        public List<Image> blobs;
        
        //private variables
        private List<Vector3> _positions;
        private int _selection;
        
        public override void Initialize() {
            startButton.onClick.AddListener(HandleLoad);

            _positions = new List<Vector3>();
            foreach (var blob in blobs) {
                _positions.Add(new Vector3());
            }
            
            mapDropdown.onValueChanged.AddListener(selection => {
                _selection = selection;
            });
        }

        private void Update() {
            var step = Time.deltaTime * 100f;
            for(var i = 0; i < blobs.Count; i ++) {
                var blobTransform = blobs[i].gameObject.transform;
                blobTransform.position = Vector3.MoveTowards(blobTransform.position, _positions[i], step);
            }
        }

        protected override void OnShow() {
            StartCoroutine(AnimateBlobs());
        }

        private void HandleLoad() {
            AppManager.Instance.LoadMap(AppManager.Instance.mapFiles[_selection]);
            ViewManager.Show<SimulatorView>();
        }

        private IEnumerator AnimateBlobs() {
            while (true) {
                for (var i = 0; i < _positions.Count; i++)
                    _positions[i] = new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), 0);
                Debug.Log("picked new positions");
                yield return new WaitForSeconds(2);
            }
        }
    }
}
