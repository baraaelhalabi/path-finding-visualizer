using UnityEngine.Events;

namespace UI.ViewManagement {
    public interface ILoaderView {
        UnityEvent OnFinishedLoading {
            get;
        }
    }
}