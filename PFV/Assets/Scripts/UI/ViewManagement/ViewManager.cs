using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UI.ViewManagement {
    public class ViewManager : MonoBehaviour {
        public static readonly UnityEvent<string> ViewOpened = new(); 

        private static ViewManager _instance;
    
        [SerializeField] private View startingView;
        private View _currentView;
    
        [SerializeField] private View[] views;
        private readonly Stack<View> _history = new();

        private void Awake() {
            if (_instance != null) return;
            _instance = this;
        }
    
        public static T GetView<T>() where T : View {
            foreach (var view in _instance.views)
                if (view is T tView)
                    return tView;
            return null;
        }

        public static void Show<T>(bool remember = true) where T : View {
            foreach (var view in _instance.views) {
                if (view is not T) continue;
                if (_instance._currentView != null) {
                    if (remember) _instance._history.Push(_instance._currentView);
                    _instance._currentView.Hide();
                }
                view.Show();
                _instance._currentView = view;
            }
        }

        private static void Show(View view, bool remember = true) {
            if (_instance._currentView != null) {
                if (remember) _instance._history.Push(_instance._currentView);
                _instance._currentView.Hide();
            }
            view.Show();
            _instance._currentView = view;
        }

        public static void ShowLast() {
            if (_instance._history.Count == 0) return;
            Show(_instance._history.Pop(), false);
        }

        private void InitializeViews(bool dismissLoader = false) {
            foreach (var view in views) {
                if (view is ILoaderView && dismissLoader) continue;
                view.Initialize();
                view.Hide();
            }
        }

        private void Start() {
            if (startingView != null && startingView is ILoaderView view) {
                view.OnFinishedLoading.AddListener(() => {
                    InitializeViews(true);
                });
                startingView.Initialize();
                Show(startingView, false);
                return;
            }
        
            InitializeViews();

            if (startingView != null)
                Show(startingView, false);
        }
    }
}
