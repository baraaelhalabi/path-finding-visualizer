using System;
using DataStructures;
using UnityEngine;

namespace UI.ViewManagement {
    public abstract class View : MonoBehaviour {
        public abstract void Initialize();

        public virtual void Setup() { }

        public virtual void Dismantle() { }

        protected virtual void OnHide() { }

        protected virtual void OnShow() { }

        public virtual void Show() {
            gameObject.SetActive(true);
            OnShow();
        }

        public virtual void Hide() {
            OnHide();
            gameObject.SetActive(false);
        }

        private void OnDisable() {
            StopAllCoroutines();
        }
    }
}
