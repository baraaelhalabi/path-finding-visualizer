using System;
using System.Xml;

namespace Utility {
    public static class Xml {
        public static T GetAttribute<T>(this XmlAttributeCollection attributes, string attrName) {
            var strValue = attributes[attrName].Value;
            return (T)Convert.ChangeType(strValue, typeof(T));
        }
    }
}