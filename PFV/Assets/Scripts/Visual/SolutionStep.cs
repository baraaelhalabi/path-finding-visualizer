using System.Linq;
using Core;
using UnityEngine;

namespace Visual {
    public class SolutionStep : VisualizationStep {
        public override void Render() {
            var mesh = MeshFilter.mesh;
        
            if(mesh == null) mesh = new Mesh();
            var vertices = mesh.vertices.ToList();
            var triangles = mesh.triangles.ToList();
            var segments = vertices.Count / 4;
        
            var p1 = Node1.WorldPosition;
            var p2 = Node2.WorldPosition;
            var rotation = Quaternion.LookRotation(p2 - p1).normalized;
            vertices.Add(p1 + rotation * Vector3.left * Thickness / 2f);
            vertices.Add(p1 + rotation * Vector3.right * Thickness / 2f);
            vertices.Add(p2 + rotation * Vector3.left * Thickness / 2f);
            vertices.Add(p2 + rotation * Vector3.right * Thickness / 2f);
                
            triangles.Add(0 + 4 * segments);
            triangles.Add(2 + 4 * segments);
            triangles.Add(1 + 4 * segments);
            triangles.Add(1 + 4 * segments);
            triangles.Add(2 + 4 * segments);
            triangles.Add(3 + 4 * segments);

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();

            MeshFilter.mesh = mesh;
            
            StatsManager.Instance.AddDistance(Vector3.Distance(Node1.WorldPosition, Node2.WorldPosition) / 1.07968f);
        }
    }
}
