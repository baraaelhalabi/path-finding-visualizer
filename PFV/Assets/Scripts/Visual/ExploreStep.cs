using System.Linq;
using UnityEngine;

namespace Visual {
    public class ExploreStep : VisualizationStep {
        public override void Render() {
            var time = Time.time;
            var mesh = MeshFilter.mesh;
        
            if(mesh == null) mesh = new Mesh();
            var vertices = mesh.vertices.ToList();
            var triangles = mesh.triangles.ToList();
            var uv2 = mesh.uv2.ToList();
            var segments = vertices.Count / 4;
        
            var p1 = Node1.WorldPosition;
            var p2 = Node2.WorldPosition;
            var rotation = Quaternion.LookRotation(p2 - p1).normalized;
            vertices.Add(p1 + rotation * Vector3.left * Thickness / 2f);
            vertices.Add(p1 + rotation * Vector3.right * Thickness / 2f);
            vertices.Add(p2 + rotation * Vector3.left * Thickness / 2f);
            vertices.Add(p2 + rotation * Vector3.right * Thickness / 2f);
                
            triangles.Add(0 + 4 * segments);
            triangles.Add(2 + 4 * segments);
            triangles.Add(1 + 4 * segments);
            triangles.Add(1 + 4 * segments);
            triangles.Add(2 + 4 * segments);
            triangles.Add(3 + 4 * segments);
        
            //uvs used to pass a timestamp
            uv2.AddRange(Enumerable.Repeat(new Vector2(time, 0), 4));

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.uv2 = uv2.ToArray();

            MeshFilter.mesh = mesh;
        }
    }
}
