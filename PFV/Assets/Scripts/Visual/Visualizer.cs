using System.Collections.Generic;
using Core;
using Graph.DataStructures;
using Singleton;
using Sounds;
using UnityEngine;

namespace Visual {
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Visualizer : MonoBehaviourSingleton<Visualizer> {
        [Header("Visualization Settings")]
        public float thickness;
        public Material highlightMaterial;
        public Material solutionMaterial;
        public Color startColor, endColor;
        public float stepDuration;
        public float fadeDuration = 1;

        [Header("Solution Visualization Settings")]
        public MeshFilter solutionMeshFilter;
    
        private MeshFilter _meshFilter;
        private MeshRenderer _meshRenderer;
        private Mesh _mesh;
        // private int _count;
        private float _lastStepTime;

        private Queue<VisualizationStep> _visualizationBuffer;
    
        private static readonly int StartColor = Shader.PropertyToID("_StartColor");
        private static readonly int EndColor = Shader.PropertyToID("_EndColor");
        private static readonly int FadeDuration = Shader.PropertyToID("_FadeDuration");
    
        protected override void SingletonAwake() {
            _meshFilter = GetComponent<MeshFilter>();
            _meshRenderer = GetComponent<MeshRenderer>();
        
            highlightMaterial.SetColor(StartColor, startColor);
            highlightMaterial.SetColor(EndColor, endColor);
            highlightMaterial.SetFloat(FadeDuration, fadeDuration);

            _visualizationBuffer = new Queue<VisualizationStep>();
        }

        private void Update() {
            if (Time.time - _lastStepTime < stepDuration || _visualizationBuffer is not { Count: > 0 }) return;
            _lastStepTime = Time.time;

            var step = _visualizationBuffer.Dequeue();
            if(step is ExploreStep) StatsManager.Instance.AddEdge();
            step?.Render();
            SoundManager.Instance.PlayExploreSound();
            
            if (_visualizationBuffer.Count != 0) return;
            SoundManager.Instance.exploreAudioSource.Stop();
            SoundManager.Instance.PlaySolutionSoundEffect();
            Glow();
        }

        public void AddStep(Node node1, Node node2) {
            _visualizationBuffer.Enqueue(new ExploreStep {
                Node1 = node1,
                Node2 = node2,
                Thickness = thickness,
                MeshFilter = _meshFilter
            });
        }

        private void Glow() {
            var time = Time.time;
            var uv2 = _meshFilter.mesh.uv2;
            for (var i = 0; i < uv2.Length; i++) {
                uv2[i].x = time + .5f;
            }
            _meshFilter.mesh.uv2 = uv2;
        }

        public void AddSolutionStep(Node node1, Node node2) {
            _visualizationBuffer.Enqueue(new SolutionStep {
                Node1 = node1,
                Node2 = node2,
                Thickness = thickness,
                MeshFilter = solutionMeshFilter
            });
        }

        public void Reset() {
            _meshFilter.mesh = new Mesh();
            solutionMeshFilter.mesh = new Mesh();
            _visualizationBuffer = new Queue<VisualizationStep>();
        }
    }
}
