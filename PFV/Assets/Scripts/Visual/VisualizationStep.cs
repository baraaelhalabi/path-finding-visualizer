using Graph.DataStructures;
using UnityEngine;

namespace Visual {
    public abstract class VisualizationStep {
        public Node Node1 { get; set; }
        public Node Node2 { get; set; }
        public float Thickness { get; set; }
        public MeshFilter MeshFilter { get; set; }
        public abstract void Render();
    }
}
