using System.Threading.Tasks;
using Algorithms;
using Graph.Builder;
using Input;
using OSM.DataStructures;
using OSM.Loader;
using OSM.Renderer;
using Singleton;
using Sounds;
using UnityEngine;
using Visual;

namespace Core {
    public class AppManager: MonoBehaviourSingleton<AppManager> {
        [Header("Temporary Settings")]
        public TextAsset[] mapFiles;
        
        public OpenStreetMap Map { get; private set; }
        public Graph.DataStructures.Graph Graph { get; private set; }
        
        public IPathFinder Algorithm { get; set; }

        public async void Solve() {
            Graph.Reset();
            StatsManager.Instance.Reset();
            SoundManager.Instance.Reset();
            Visualizer.Instance.Reset();
            if (Algorithm == null) return;
            string startNodeId, targetNodeId;
            (startNodeId, targetNodeId) = InputSystem.Instance.GetWayPointIds();
            var time = Time.time;
            await Task.Run(() => Algorithm.Solve(Graph.GetById(startNodeId), Graph.GetById(targetNodeId)));
            StatsManager.Instance.SetTime((Time.time - time) * 1000);
        }

        public void LoadMap(TextAsset file) {
            Map = OsmLoader.LoadMap(file);
            Graph = GraphBuilder.ToGraph(Map);
            
            //TODO refactor this later
            OsmRenderer.Instance.Render(Map);
            InputSystem.Instance.SetupInput(Graph);
            Visualizer.Instance.Reset();
        }
    }
}
