using Singleton;

namespace Core {
    public class StatsManager : MonoBehaviourSingleton<StatsManager> {
        public event OnEdgesExploredChangedHandler OnEdgeExploredChanged; 
        public delegate void OnEdgesExploredChangedHandler(int edges);
        
        public event OnTimeChangedHandler OnTimeChanged; 
        public delegate void OnTimeChangedHandler(float time);
        
        public event OnDistanceChangeHandler OnDistanceChange; 
        public delegate void OnDistanceChangeHandler(float distance);
        
        private int ExploredEdges { get; set; }
        private float Time { get; set; }
        private float Distance { get; set; }

        public void Reset() {
            SetExploredEdges(0);
            SetTime(0);
            SetDistance(0);
        }

        private void SetExploredEdges(int edges) {
            ExploredEdges = edges;
            OnEdgeExploredChanged?.Invoke(ExploredEdges);
        }

        public void AddEdge() {
            ExploredEdges++;
            OnEdgeExploredChanged?.Invoke(ExploredEdges);
        }

        public void SetTime(float time) {
            Time = time;
            OnTimeChanged?.Invoke(Time);
        }

        private void SetDistance(float distance) {
            Distance = distance;
            OnDistanceChange?.Invoke(Distance);
        }

        public void AddDistance(float value) {
            Distance += value;
            OnDistanceChange?.Invoke(Distance);
        }
    }
}
