using System;
using System.Collections.Generic;
using Graph.DataStructures;
using Singleton;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Input {
    public class InputCell {
        public List<Node> Nodes { get; } = new();

        public Node GetClosestNode(Vector2 cursorPosition) {
            if (Nodes is { Count: 0 }) return null;
            var cam = Camera.main;
            if (cam == null) return null;
            
            var result = Nodes[0];
            var shortestDistance = Vector2.Distance(cursorPosition, cam.WorldToScreenPoint(Nodes[0].WorldPosition));
            foreach (var node in Nodes) {
                var distance = Vector2.Distance(cursorPosition, cam.WorldToScreenPoint(node.WorldPosition));
                if (distance >= shortestDistance) continue;
                
                shortestDistance = distance;
                result = node;
            }

            return result;
        }
    }
    
    public class InputSystem : MonoBehaviourSingleton<InputSystem> {
        [Header("Input settings")]
        [Range(5, 20)]
        public int rows;
        [Range(5, 20)]
        public int cols;
        
        [Header("WayPoints")]
        public GameObject[] wayPoints;

        private Camera _cam;
        private GameObject _target;
        private InputCell[,] _grid;
        private float _cellWidth, _cellHeight;
        private string _startNodeId;
        private string _endNodeId;

        public void SetupInput(Graph.DataStructures.Graph graph) {
            if (_cam == null) {
                Debug.LogWarning("No camera found, cannot setup input");
                return;
            }

            _grid = new InputCell[rows, cols];
            for (var i = 0; i < rows; i++)
                for (var j = 0; j < cols; j++)
                    _grid[i, j] = new InputCell();
        
            var width = Screen.width;
            var height = Screen.height;

            _cellWidth = width / (float)cols; 
            _cellHeight = height / (float)rows;

            foreach (var node in graph.Nodes) {
                var screenPosition = _cam.WorldToScreenPoint(node.WorldPosition);
                var row = Math.Clamp((int)(screenPosition.y / _cellHeight), 0, rows - 1);
                var col = Math.Clamp((int)(screenPosition.x / _cellWidth), 0, cols - 1);
            
                _grid[row, col].Nodes.Add(node);
            }

            RandomizeWayPoints(graph);
        }

        private void RandomizeWayPoints(Graph.DataStructures.Graph graph) {
            Node node1, node2;
            do {
                node1 = graph.Nodes[Random.Range(0, graph.Nodes.Count)];
                node2 = graph.Nodes[Random.Range(0, graph.Nodes.Count)];
            } while (node1 == node2 || !InScreenSpace(node1) || !InScreenSpace(node2));

            wayPoints[0].transform.position = new Vector3(node1.WorldPosition.x, .4f, node1.WorldPosition.z);
            wayPoints[1].transform.position = new Vector3(node2.WorldPosition.x, .4f, node2.WorldPosition.z);
            _startNodeId = node1.Id;
            _endNodeId = node2.Id;
        }

        private bool InScreenSpace(Node node) {
            var screenPosition = _cam.WorldToScreenPoint(node.WorldPosition);
            return screenPosition.x > 0 && screenPosition.x < Screen.width && screenPosition.y > 0 && screenPosition.y < Screen.height;
        }

        private void Start() {
            _cam = Camera.main;
        }

        private void Update() {
            if (_grid == null) return;
            var mousePosition = UnityEngine.Input.mousePosition;
            
            CheckMouseDown(mousePosition);
            
            CheckMouseUp();
            
            MoveWayPoint(mousePosition);
        }

        private void MoveWayPoint(Vector3 mousePosition) {
            if (_target == null) return;
            var row = Math.Clamp((int)(mousePosition.y / _cellHeight), 0, rows - 1);
            var col = Math.Clamp((int)(mousePosition.x / _cellWidth), 0, cols - 1);

            var node = _grid[row, col].GetClosestNode(mousePosition);
            if (node == null) return;
            _target.transform.position = new Vector3(node.WorldPosition.x, .4f, node.WorldPosition.z);
            if (_target == wayPoints[0]) _startNodeId = node.Id;
            else _endNodeId = node.Id;
        }

        private void CheckMouseUp() {
            if (UnityEngine.Input.GetMouseButtonUp(0)) {
                _target = null;
            }
        }

        private void CheckMouseDown(Vector3 mousePosition) {
            if (UnityEngine.Input.GetMouseButtonDown(0)) {
                var ray = _cam.ScreenPointToRay(mousePosition);
                var hits = Physics.RaycastAll(ray, _cam.gameObject.transform.position.y + 1);

                foreach (var hit in hits) {
                    if (!hit.collider.CompareTag("WayPoint")) continue;
                    _target = hit.collider.gameObject;
                }
            }
        }

        public (string, string) GetWayPointIds() {
            return (_startNodeId, _endNodeId);
        }
    }
}
